# This repo is for issues tracking only.

# Sources available at: https://repo.4neko.org/aienma.git

# Aienma ![alt text](https://gitlab.com/4neko/aienma/-/raw/master/logo_600.png?ref_type=heads&inline=true)

A previously called 'logdaemon'. For this reason a main config file is called `logdaemon.shm`.

A software developed on Rust language, which is fetching UTF8 data from log files on disk or redirected messages from syslog or other source and compares received log lines agains the rules and may trigger an action if the amount of violations was reached.

It can be used to defend against bruteforce attacks or monitor special events in the system.

The sensor destinguish between what type of `entity` (a detected usefull payload which can be host, username) it is dealing with. This program can be used to block or limit both users or hosts. At the moment it does not able to resolve the domain names and domain names can not be filtered.


Supported input (log formats):
- JSON formatted messages (compact)
- text logs (syslog formatted, other format)

OS:
- GNU/Linux (deps: Inotify)
- FreeBSD (deps: KQueue)
- OpenBSD (deps: KQueue)
- NetBSD (deps: KQueue)
- Mac OSX, OSX (deps: AGCD (Apple Grand Cental Dispatch), BSD KQueue)
- HardenedBSD (deps: KQueue)
- DragonflyBSD (deps: KQueue)

There is no Windows support.

Data storage engines:
- File
- Mysql
- Sqlite

Notification:
- email
- external program

Native action drivers:
- pf (FreeBSD only)
- ipfw (FreeBSD only)

The storage engine is needed to store session data and detection records. Session data is a blocked `entities`. Detection data is a data which later can
be used for further analytics.

Native action drivers:
- FreeBSD pf (firewall)
- FreeBSD ipfw (firewall) (untested)

Requires: 
Rust version minimum: 1.68-stable

# Documentaion
See `/doc/` directory.

# Daemon control
[Aienmactl](https://gitlab.com/relkom/aienma_cli)

# Project layout

```text
src
 ├─actions
 ├─config
 │  ├─etc
 │  └─structures
 ├─control
 │  └─communication
 ├─databases
 ├─interpritator
 ├─notify
 ├─portable
 ├─priv_sys
 ├─shared
 │  ├─config
 │  └─exec
 └─sys
    ├─entity_worker
    ├─log_input
    ├─log_sources
    ├─parser
    └─test
```

> /src/ - a source code of the daemon  
> /actions/ - a native drivers for the actions 
> /config/ - a configuration structures  
> /control/ - a remote control  
> /databases/ - a database handlers  
> /interpritator/ - an interpritator and translator of scheme  
> /notify/ - a native handlers for the notifications  
> /portable/ - a portable code  
> /priv_sys/ - a code which used by the privileged portion  
> /shared/ - a shared code  
> /sys/ - a code which used by unpriv portion  

# ToDo

- capsicum (FreeBSD, Linux?), pledge (OpenBSD)
- add scripting for complex rules (Lua? Lisp?)
- Optimizations, code cleanup

# Current status
Logdaemon V 1.0.0-RELEASE
